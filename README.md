## Considerations

Given the requirements of the app I didn't see necessary to use complex state management libraries such as Redux which is why I went with this _naiver_ approach since where it's most complex is on the auth side.

Test server doesn't seem to have any CORS set up, so I created a proxy which also deals with handling the JWT and storing it with a secure Cookie that can't be read by the client to avoid any CSRF or XSS attacks (though it's a moot point given it's an exercise, but it's still good practice).

If the user has JWT it'll be sent with requests transparently to the Front End though this hasn't been required. In a more realistic scenario I would have gone with a middleware on Express.

## Instructions

Install dependencies either with NPM or Yarn and then run the script `start` such as:

````shell
npm start
````

Finally open `http://localhost:8080/` on your browser
