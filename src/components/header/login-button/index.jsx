import * as React from 'react';
import { Link } from 'react-router-dom';
import AuthAwareContent from '../../content/auth-aware-content';
import { AuthContext } from '../../../context/auth';

const { useContext } = React;

export default function LoginButton() {
  const authContext = useContext(AuthContext);
  const handleLogoutButton = async () => {
    try {
      await authContext.logout();
    } catch (e) {
      // Error silently
    }
  };

  return (
    <AuthAwareContent
      isLogged={(
        <li className="logged-in menu-item menu-item-type-custom menu-item-object-custom menu-item-1915">
          <button
            className="button-link"
            type="button"
            onClick={handleLogoutButton}>
            Logout
          </button>
        </li>
      )}
      isAnonymous={(
        <li className="logged-out menu-item menu-item-type-custom menu-item-object-custom menu-item-1915">
          <Link to="/login">Login</Link>
        </li>
      )}
    />
  );
}
