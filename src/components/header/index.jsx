import PropTypes from 'prop-types';
import { Link, useLocation } from 'react-router-dom';
import classnames from 'classnames';
import LoginButton from './login-button';

function Header({ menu }) {
  const currentLocation = useLocation();

  return (
    <header
      className="site-header"
      role="banner"
      itemScope="itemscope"
      itemType="http://schema.org/WPHeader">
      <h1 className="site-title" itemScope itemType="http://schema.org/Organization">
        10up Blog
      </h1>

      <nav
        className="site-navigation"
        role="navigation"
        itemScope="itemscope"
        itemType="http://schema.org/SiteNavigationElement">

        <a href="#menu-main-nav" id="js-menu-toggle" className="site-menu-toggle">
          <span className="screen-reader-text">Primary Menu</span>
          <span aria-hidden="true">☰</span>
        </a>

        <ul id="menu-main-nav" className="primary-menu">
          {menu.map(menuItem => {
            const cx = classnames('menu-item menu-item-type-custom menu-item-object-custom', {
              'current-menu-item': currentLocation.pathname === menuItem.path
            });

            return (
              <li className={cx} key={menuItem.name}>
                <Link to={menuItem.path}>{menuItem.name}</Link>
              </li>
            );
          })}
          <LoginButton />
        </ul>
      </nav>
    </header>
  );
}

Header.propTypes = {
  menu: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Header;
