import * as React from 'react';
import PropTypes from 'prop-types';
import { AuthContext } from '../../../context/auth';

const { useContext } = React;

function AuthAwareContent({ isLogged, isAnonymous }) {
  const authContext = useContext(AuthContext);

  if (authContext.is('Logged')) {
    return isLogged;
  }

  return isAnonymous;
}

AuthAwareContent.propTypes = {
  isLogged: PropTypes.element,
  isAnonymous: PropTypes.oneOfType([PropTypes.element, PropTypes.bool])
};

AuthAwareContent.defaultProps = {
  isLogged: null,
  isAnonymous: null
};

export default AuthAwareContent;
