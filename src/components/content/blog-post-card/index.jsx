/* eslint-disable react/no-danger */
import PropTypes from 'prop-types';
import DisplayableDate from '../displayable-date';

function BlogPostCard({ post }) {
  const {
    title: { rendered: title },
    date,
    excerpt: { rendered: excerpt },
    author: { name: author }
  } = post;

  return (
    <article itemScope itemType="http://schema.org/BlogPosting" className="post">
      <header>
        <h2 itemProp="headline">{title}</h2>
        <DisplayableDate date={date} />

        <div className="author">
          <strong>Author</strong>: <span itemProp="author">{author}</span>
        </div>
      </header>

      <div
        itemProp="articleBody"
        className="content"
        dangerouslySetInnerHTML={{ __html: excerpt }}
      />
    </article>
  );
}

BlogPostCard.propTypes = {
  post: PropTypes.object.isRequired
};

export default BlogPostCard;
