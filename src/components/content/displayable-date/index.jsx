import PropTypes from 'prop-types';

function DisplayableDate({ text, date }) {
  const dateObject = new Date(date);
  const year = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(dateObject);
  const month = new Intl.DateTimeFormat('en', { month: 'short' }).format(dateObject);
  const day = new Intl.DateTimeFormat('en', { day: 'numeric' }).format(dateObject);

  return (
    <div className="date">
      <strong>{text}</strong>:

      <span itemProp="datePublished">
        <time dateTime={date}>{month} {day}, {year}</time>
      </span>
    </div>
  );
}

DisplayableDate.defaultProps = {
  text: 'Publish Date'
};

DisplayableDate.propTypes = {
  date: PropTypes.string.isRequired,
  text: PropTypes.string
};

export default DisplayableDate;
