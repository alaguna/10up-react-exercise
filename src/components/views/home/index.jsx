import * as React from 'react';
import PropTypes from 'prop-types';
import AuthAwareContent from '../../content/auth-aware-content';
import BlogPostCard from '../../content/blog-post-card';
import { AuthContext } from '../../../context/auth';

const { useContext } = React;

function HomeView({ posts }) {
  const authContext = useContext(AuthContext);

  return (
    <>
      <AuthAwareContent
        isLogged={(
          <section className="welcome logged-in">
            Welcome {authContext.user.user_nicename}!
          </section>
        )}
      />
      <div itemScope itemType="https://schema.org/Blog">
        {posts.map(post => (
          <BlogPostCard post={post} key={post.id} />
        ))}
      </div>
    </>
  );
}

HomeView.propTypes = {
  posts: PropTypes.array.isRequired
};

export default HomeView;
