/* eslint-disable react/no-danger */
import PropTypes from 'prop-types';
import useTitle from '../../../hooks/use-title';

function StaticPageView({ page }) {
  const {
    title: { rendered: title },
    content: { rendered: content }
  } = page;

  useTitle(title);

  return (
    <>
      <h1>{title}</h1>
      <div className="page" dangerouslySetInnerHTML={{ __html: content }} />
    </>
  );
}

StaticPageView.propTypes = {
  page: PropTypes.object.isRequired
};

export default StaticPageView;
