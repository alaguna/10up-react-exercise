import PropTypes from 'prop-types';
import NotFound from '../../../pages/not-found';

function DynamicViewWrapper({
  isLoading,
  loadingText,
  component,
  validate,
  ...props
}) {
  if (isLoading) {
    return (<p>{loadingText}</p>);
  }

  const RenderableComponent = component;

  if (validate) {
    const { [validate]: validableProperty } = props;

    if (!validableProperty) {
      return (<NotFound />);
    }
  }

  return (<RenderableComponent {...props} />);
}

DynamicViewWrapper.defaultProps = {
  loadingText: 'Loading',
  isLoading: false,
  children: null,
  validate: ''
};

DynamicViewWrapper.propTypes = {
  isLoading: PropTypes.bool,
  loadingText: PropTypes.string,
  validate: PropTypes.string,
  children: PropTypes.node,
  component: PropTypes.func.isRequired
};

export default DynamicViewWrapper;
