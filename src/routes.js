import PageHome from './pages/home';
import StaticPage from './pages/static-page';
import PageLogin from './pages/login';

const routes = [
  {
    path: '/',
    name: 'Home',
    exact: true,
    component: PageHome
  },
  {
    path: '/pages/:slug',
    name: 'Static Page',
    component: StaticPage
  },
  {
    path: '/login',
    name: 'Login',
    component: PageLogin
  }
];

export default routes;
