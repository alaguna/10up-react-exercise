import * as React from 'react';

const { useEffect } = React;
const BaseTitle = '10 up Blog';

export default function useTitle(title) {
  useEffect(() => {
    let newTitle = BaseTitle;

    if (title) {
      newTitle = `${BaseTitle} - ${title}`;
    }

    document.title = newTitle;
  }, []);
}
