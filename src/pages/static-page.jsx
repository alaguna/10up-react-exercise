import * as React from 'react';
import PropTypes from 'prop-types';
import { BlogContext } from '../context/blog';
import DynamicViewWrapper from '../components/views/dynamic-view-wrapper';
import StaticPageView from '../components/views/static-page';

const { useContext, useEffect } = React;

function StaticPage({ match }) {
  const [blog, blogDispatch] = useContext(BlogContext);
  const pageSlug = match.params.slug;

  useEffect(() => {
    if (!Object.keys(blog.pages).length && !blog.loading) {
      blogDispatch({ type: 'LOAD_PAGES' });
    }
  }, []);

  const page = blog.pages[pageSlug];

  return (
    <DynamicViewWrapper
      isLoading={blog.loading}
      validate="page"
      component={StaticPageView}
      page={page}
    />
  );
}

StaticPage.propTypes = {
  match: PropTypes.object.isRequired
};

export default StaticPage;
