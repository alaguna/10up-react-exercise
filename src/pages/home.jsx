import * as React from 'react';
import { BlogContext } from '../context/blog';
import useTitle from '../hooks/use-title';
import DynamicViewWrapper from '../components/views/dynamic-view-wrapper';
import HomeView from '../components/views/home';

const { useContext, useEffect } = React;

export default function PageHome() {
  useTitle();
  const [blog, blogDispatch] = useContext(BlogContext);

  useEffect(() => {
    if (!blog.posts.length && !blog.loading) {
      blogDispatch({ type: 'LOAD_POSTS' });
    }
  }, []);

  return (
    <DynamicViewWrapper
      isLoading={blog.loading}
      component={HomeView}
      posts={blog.posts}
    />
  );
}
