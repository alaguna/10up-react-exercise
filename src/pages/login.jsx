import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { AuthContext } from '../context/auth';
import AuthAwareContent from '../components/content/auth-aware-content';
import useTitle from '../hooks/use-title';

const { useState, useContext } = React;

export default function PageLogin() {
  useTitle('Login');
  const authContext = useContext(AuthContext);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const handleSubmit = async event => {
    event.preventDefault();

    try {
      await authContext.login(username, password);
    } catch (e) {
      setError(e.message);
    }
  };

  let errorEl = null;

  if (error) {
    errorEl = (
      <div className="error" dangerouslySetInnerHTML={{ __html: error }} />
    );
  }

  const loginForm = (
    <div className="login">
      <form method="post" autoComplete="off" onSubmit={handleSubmit}>
        <div>
          <label htmlFor="username">Username</label>
          <input
            id="username"
            type="text"
            name="username"
            value={username}
            required
            onChange={e => setUsername(e.target.value)}
          />
        </div>
        <div>
          <label htmlFor="password">Password</label>
          <input
            id="password"
            type="password"
            name="password"
            value={password}
            required
            onChange={e => setPassword(e.target.value)}
          />
        </div>
        <div>
          <input type="submit" value="Submit" />
        </div>
      </form>
      {errorEl}
    </div>
  );

  return <AuthAwareContent isAnonymous={loginForm} isLogged={<Redirect to="/" />} />;
}
