import * as React from 'react';
import * as BlogService from '../services/blog';

const BlogContext = React.createContext();
BlogContext.displayName = 'BlogContext';

const { useReducer } = React;

function asyncMiddleware(dispatch) {
  return action => {
    switch (action.type) {
      case 'LOAD_POSTS':
        dispatch({ type: 'TOGGLE_LOADING' });
        BlogService.getPosts()
          .then(payload => dispatch({ type: action.type, payload }));
        break;
      case 'LOAD_PAGES':
        dispatch({ type: 'TOGGLE_LOADING' });
        BlogService.getPages()
          .then(pages => {
            const payload = {};
            pages.forEach(page => {
              payload[page.slug] = page;
            });

            dispatch({ type: action.type, payload });
          });
        break;
      default:
        dispatch(action);
    }
  };
}

function blogReducer(state, action) {
  switch (action.type) {
    case 'LOAD_POSTS':
      return { ...state, posts: [...action.payload], loading: false };
    case 'LOAD_PAGES':
      return { ...state, pages: { ...action.payload }, loading: false };
    case 'TOGGLE_LOADING':
      return { ...state, loading: !state.loading };
    default:
      return state;
  }
}

const initialState = {
  posts: [],
  pages: {},
  loading: false
};

function BlogProvider({ ...rest }) {
  const [state, dispatch] = useReducer(blogReducer, initialState);

  return (
    <BlogContext.Provider
      value={[state, asyncMiddleware(dispatch)]}
      {...rest}
    />
  );
}

export { BlogContext, BlogProvider };
