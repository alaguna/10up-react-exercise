import * as React from 'react';
import * as AuthService from '../services/auth';

const AuthContext = React.createContext();
AuthContext.displayName = 'AuthContext';

const { useState, useEffect, useCallback } = React;

const defaultState = {
  isAnon: true,
  isLogged: false
};

const states = {
  logged: { ...defaultState, isAnon: false, isLogged: true },
  anon: { ...defaultState }
};

function AuthProvider({ ...rest }) {
  const [user, setUser] = useState({ ...states.anon });
  const [isLoading, setLoading] = useState(true);
  const isHelper = state => !isLoading && user[`is${state}`];
  const login = useCallback(async (username, password) => {
    const userData = await AuthService.login(username, password);
    setUser({ ...states.logged, ...userData });
    return userData;
  }, []);
  const logout = useCallback(async () => {
    try {
      await AuthService.logout();
      setUser({ ...states.anon });
    } catch (e) {
      // Error silently
    }
  }, []);

  useEffect(() => {
    (async () => {
      const hasUserCookie = document.cookie.match(/^(.*;)?\s*ten_up_user\s*=\s*[^;]+(.*)?$/);

      if (hasUserCookie) {
        try {
          const userData = await AuthService.validate();
          setUser({ ...states.logged, ...userData });
        } catch (e) {
          // Error silently
        }
      }

      setLoading(false);
    })();
  }, []);

  return (
    <AuthContext.Provider
      value={{
        isLoading,
        user,
        login,
        logout,
        is: isHelper
      }}
      {...rest}
    />
  );
}

export { AuthProvider, AuthContext };
