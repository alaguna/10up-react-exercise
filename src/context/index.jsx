import PropTypes from 'prop-types';
import { BrowserRouter as Router } from 'react-router-dom';
import { AuthProvider } from './auth';
import { BlogProvider } from './blog';

export default function AppProviders({ children }) {
  return (
    <Router>
      <AuthProvider>
        <BlogProvider>
          {children}
        </BlogProvider>
      </AuthProvider>
    </Router>
  );
}

AppProviders.propTypes = {
  children: PropTypes.node.isRequired
};
