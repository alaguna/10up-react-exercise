import apiCall from './api-wrapper';

function login(username, password) {
  return apiCall('/api/login', 'POST', {
    username,
    password
  });
}

function validate() {
  return apiCall('/api/validate', 'POST');
}

function logout() {
  return apiCall('/api/logout', 'POST');
}

export { login, validate, logout };
