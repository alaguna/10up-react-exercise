export default async function apiCall(path, method = 'GET', body) {
  let response;
  let data;
  const settings = {
    method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  };

  if (typeof body !== 'undefined') {
    settings.body = JSON.stringify(body);
  }

  try {
    response = await fetch(path, settings);
    data = await response.json();
  } catch (e) {
    return e;
  }

  if (response.status > 200) {
    throw new Error(data.message);
  }

  return data;
}
