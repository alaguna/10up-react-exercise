import apiCall from './api-wrapper';

function getUser(author) {
  return apiCall(`/api/user/${author}`);
}

function getPages() {
  return apiCall('/api/pages');
}

async function getPosts() {
  const posts = await apiCall('/api/posts');
  const authorsId = new Set();

  posts.forEach(post => authorsId.add(post.author));
  const authors = {};

  await Promise.all([...authorsId].map(id => getUser(id))).then(authorsArray => {
    authorsArray.forEach(author => {
      authors[author.id] = author;
    });
  });

  return posts.map(post => {
    let author = null;

    if (authors[post.author]) {
      author = { ...authors[post.author] };
    }

    return { ...post, author };
  });
}

export { getPosts, getUser, getPages };
