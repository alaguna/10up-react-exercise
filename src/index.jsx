import ReactDOM from 'react-dom';
import { Switch, Route } from 'react-router-dom';
import Header from './components/header';
import routes from './routes';
import AppProviders from './context';
import './styles/index.css';
import NotFound from './pages/not-found';

function App() {
  const menu = [
    {
      name: 'Home',
      path: '/'
    },
    {
      name: 'About',
      path: '/pages/about-us'
    }
  ];

  return (
    <AppProviders>
      <Header menu={menu} />
      <Switch>
        {routes.map(route => (
          <Route
            key={route.name}
            {...route}
          />
        ))}
        <Route path="*"><NotFound/></Route>
      </Switch>
    </AppProviders>
  );
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
