const fs = require('fs');
const path = require('path');

module.exports = function assetsMiddleware(manifestPath, options) {
  let manifest;
  let isManifestLoaded = false;

  const middlewareSettings = {
    devMode: false,
    ...options
  };

  function loadManifest() {
    let data = {};

    try {
      if (fs.statSync(manifestPath).isDirectory()) {
        const manifestFiles = fs.readdirSync(manifestPath);

        if (manifestFiles.length === 0) {
          console.error('There are no asset manifests');
        }

        manifestFiles.forEach(m => {
          const manifestContent = JSON.parse(fs.readFileSync(path.resolve(manifestPath, m), 'utf8'));
          data = { ...data, manifestContent };
        });
      } else {
        data = JSON.parse(fs.readFileSync(manifestPath, 'utf8'));
      }

      isManifestLoaded = true;
    } catch (e) {
      console.log('Could not load asset manifest', e);
    }

    return data;
  }

  function getAsset(assetPath) {
    if (middlewareSettings.devMode || !isManifestLoaded) {
      manifest = loadManifest();
    }

    if (manifest) {
      return `/${manifest[assetPath]}`;
    }

    return assetPath;
  }

  return function webpackAsset(req, res, next) {
    res.locals.webpack_asset = getAsset;
    next();
  };
};
