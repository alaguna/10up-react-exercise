const requestHelper = require('../utils/json-request');
const decodeJWTPayload = require('../utils/jwt-payload');
const urlToHTPPParams = require('../utils/url-to-http-param');

const PATH = 'https://js1.10up.com/wp-json/jwt-auth/v1/token';
const UrlParams = urlToHTPPParams(PATH);

module.exports = async (req, res) => {
  const { username, password } = req.body;
  const response = {
    status: 200,
    body: {}
  };

  if (!username || !password) {
    response.status = 400;
    response.body = {
      message: '<strong>Error</strong>: Username and password are required.'
    };
  }

  const requestBody = JSON.stringify({
    username,
    password
  });

  const serverResponse = await requestHelper({
    ...UrlParams,
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': requestBody.length
    },
    method: 'POST',
    body: requestBody
  });

  if (serverResponse) {
    if (serverResponse.token) {
      const payload = decodeJWTPayload(serverResponse.token);

      if (payload) {
        const cookieExpireDate = new Date(payload.exp * 1000);
        response.body = {
          user_email: serverResponse.user_email,
          user_nicename: serverResponse.user_nicename,
          user_display_name: serverResponse.user_display_name
        };

        res.cookie('ten_up_jwt', serverResponse.token, {
          expires: cookieExpireDate,
          httpOnly: true
        });
        res.cookie('ten_up_user', JSON.stringify(response.body));
      }
    } else { // Error
      const { message, data } = serverResponse;
      response.status = data?.status || 400;
      response.body = { message };
    }
  }

  res.status(response.status).send(response.body);
};
