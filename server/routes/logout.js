module.exports = (req, res) => {
  res.clearCookie('ten_up_jwt');
  res.clearCookie('ten_up_user');

  res.sendStatus(200);
};
