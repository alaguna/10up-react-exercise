const requestHelper = require('../utils/json-request');
const urlToHTPPParams = require('../utils/url-to-http-param');

const PATH = 'https://js1.10up.com/wp-json/jwt-auth/v1/token/validate';
const UrlParams = urlToHTPPParams(PATH);

module.exports = async (req, res) => {
  const response = {
    status: 200,
    body: {}
  };

  if (req.cookies && req.cookies.ten_up_jwt) {
    const serverResponse = await requestHelper({
      ...UrlParams,
      method: 'POST'
    }, req.cookies.ten_up_jwt);

    if (serverResponse) {
      response.status = serverResponse.data?.status || 401;

      if (response.status === 200) {
        response.body = JSON.parse(req.cookies.ten_up_user);
      }
    }
  } else {
    response.status = 401;
    response.body.message = 'No token';
  }

  res.status(response.status).send(response.body);
};
