const API_PATH = 'https://js1.10up.com/wp-json/wp/v2/posts';
const jsonProxyHelper = require('../utils/json-proxy');

module.exports = jsonProxyHelper(API_PATH);
