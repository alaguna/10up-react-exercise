const API_PATH = 'https://js1.10up.com/wp-json/wp/v2/users/';
const jsonProxyHelper = require('../utils/json-proxy');

module.exports = (req, res) => {
  const url = `${API_PATH}${req.params.user}`;
  const handler = jsonProxyHelper(url);

  return handler(req, res);
};
