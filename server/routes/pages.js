const API_PATH = 'https://js1.10up.com/wp-json/wp/v2/pages';
const jsonProxyHelper = require('../utils/json-proxy');

module.exports = jsonProxyHelper(API_PATH);
