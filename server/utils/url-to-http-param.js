module.exports = function urlToHTPPParams(url) {
  let result = {};

  if (typeof url === 'string') {
    const pathUrl = new URL(url);
    result = {
      hostname: pathUrl.host,
      path: pathUrl.pathname
    };
  }

  return result;
};
