module.exports = function decodeJWTPayload(jwt) {
  let result = null;

  if (jwt && typeof jwt === 'string') {
    const [, payload] = jwt.split('.');
    result = JSON.parse(Buffer.from(payload, 'base64').toString('ascii'));
  }

  return result;
};
