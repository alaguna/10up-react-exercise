const https = require('https');
const urlToHTPPParams = require('./url-to-http-param');

module.exports = (options, token) => new Promise(resolve => {
  let httpRequestOptions;

  if (typeof options === 'object') {
    httpRequestOptions = options;
  } else {
    httpRequestOptions = {
      ...urlToHTPPParams(options),
      method: 'GET'
    };
  }

  if (token) {
    if (typeof httpRequestOptions.headers === 'undefined') {
      httpRequestOptions.headers = {};
    }

    httpRequestOptions.headers.Authorization = `Bearer ${token}`;
  }

  const req = https.request(httpRequestOptions, res => {
    let body = '';

    res.on('data', chunk => {
      body += chunk;
    });

    res.on('end', () => {
      try {
        const json = JSON.parse(body);
        resolve(json);
      } catch (error) {
        resolve(null);
      }
    });
  }).on('error', error => {
    console.log('Error', error);
    resolve(null);
  });

  if (options.body) {
    req.write(options.body);
  }

  req.end();
});
