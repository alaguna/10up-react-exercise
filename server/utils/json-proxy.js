const requestHelper = require('./json-request');

module.exports = function jsonProxy(path) {
  return async (req, res) => {
    const data = await requestHelper(path, req.cookies?.ten_up_jwt);
    res.json(data);
  };
};
