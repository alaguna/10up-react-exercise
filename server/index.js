const express = require('express');
const bodyParser = require('body-parser');
const nunjucks = require('nunjucks');
const path = require('path');
const cookieParser = require('cookie-parser');
const PagesRoute = require('./routes/pages');
const PostsRoute = require('./routes/posts');
const UsersRoute = require('./routes/users');
const LoginRoute = require('./routes/login');
const LogoutRoute = require('./routes/logout');
const ValidateRoute = require('./routes/validate');
const assetsMiddleware = require('./middlewares/assets');

const app = express();
const port = process.env.PORT || 8080;
const isProduction = process.env.NODE_ENV === 'production';
const isDevelopment = !isProduction;

const viewsFolder = path.resolve(__dirname, './views');
const distFolder = path.resolve(__dirname, '../dist');

app
  .disable('x-powered-by')
  .use(cookieParser())
  .use(express.json())
  .use(assetsMiddleware(path.resolve(distFolder, 'assets-manifest.json'), {
    devMode: isDevelopment
  }))
  .use(bodyParser.urlencoded({
    extended: true
  }))
  .use(express.static(distFolder))
  .use(bodyParser.json())
  .set('views', viewsFolder)
  .set('view engine', 'njk')
  .get('/api/pages', PagesRoute)
  .get('/api/posts', PostsRoute)
  .get('/api/user/:user', UsersRoute)
  .post('/api/login', LoginRoute)
  .post('/api/logout', LogoutRoute)
  .post('/api/validate', ValidateRoute)
  .get('/*', (req, res) => {
    const isLogged = !!(req.cookies && req.cookies.ten_up_jwt);

    res.render('index', {
      isProduction,
      isDevelopment,
      isLogged
    });
  });

nunjucks.configure(viewsFolder, {
  autoescape: true,
  express: app,
  cache: false
});

app.listen(port, () => {
  console.log('Listening on port %s', port);
  console.log('http://localhost:%s', port);
  console.log('');
});
